package utils

import (
	"fmt"
	"log"
)

func Panic(err error, msg interface{}) {
	if err != nil {
		errMsg := fmt.Errorf("%s\n%s", msg, err)
		log.Fatal(errMsg)
	}
}
