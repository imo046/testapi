package main

import (
	"example.com/testapi/v2/mongo"
	"example.com/testapi/v2/utils"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

func initExample(connection string) {
	mongoClient, err := mongo.NewMongoClient(connection)
	utils.Panic(err, "Failed to initialize the client")

	err = mongoClient.PingServer()
	utils.Panic(err, "Failed to ping server")

	err = mongoClient.ConnectClient()
	utils.Panic(err, "Failed to connect to mongodb")

}

func ExampleInsertOne(m mongo.MongoDriver) {
	opt := options.InsertOneOptions{}
	insert := bson.D{bson.E{Key: "key", Value: "value"}}
	err := m.InsertOne("dbName", "collectionName", opt, insert)
	if err != nil {
		//handle error
	}
}

func ExampleInsertMany(m mongo.MongoDriver) {
	opt := options.InsertManyOptions{}
	doc1 := bson.D{bson.E{Key: "key", Value: "value"}}
	doc2 := bson.D{bson.E{Key: "key", Value: "value"}}
	insert := []interface{}{doc1, doc2}

	err := m.InsertMany("dbName", "collectionName", opt, insert)
	if err != nil {
		//handle error
	}
}

func FindOneExample(m mongo.MongoDriver) {
	opt := options.FindOptions{}

	query := bson.D{bson.E{Key: "key", Value: "value"}}
	output := map[string]interface{}{}

	err := m.FindOne("dbName", "collectionName", query, opt, &output)
	if err != nil {
		//handler error
	}
}

func FindManyExample(m mongo.MongoDriver) {
	opt := options.FindOptions{}
	opt.SetMaxTime(30 * time.Second)            //sends a Max Time in the query for a server timeout
	opt.SetLimit(0.0)                           //set the number of documents retrieved
	opt.SetProjection(bson.D{bson.E{"key", 1}}) //send a bson.D as a projection

	query := bson.D{bson.E{Key: "key", Value: "value"}}
	output := []map[string]interface{}{}

	err := m.FindOne("dbName", "collectionName", query, opt, &output)
	if err != nil {
		//handler error
	}
}

func main() {
	fmt.Println("Foo")

}
