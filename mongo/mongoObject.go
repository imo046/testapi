package mongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDriver interface {
	FindMany(database, collection string, selector bson.D, options options.FindOptions, output []interface{}) error
	FindOne(database, collection string, selector bson.D, options options.FindOptions, output interface{}) error

	InsertMany(database, collection string, options options.InsertManyOptions, insert []interface{}) error
	InsertOne(database, collection string, options options.InsertOneOptions, insert interface{}) error

	Upsert(database, collection string, selector bson.D, options options.UpdateOptions, update interface{}) error
	Update(database, collection string, selector bson.D, options options.UpdateOptions, update interface{}) error

	Delete(database, collection string, options options.DeleteOptions, selector bson.D) error
}

// Adds client functions
type MongoBasic interface {
	PingServer() error
	ConnectClient() error
	MongoDriver
}
