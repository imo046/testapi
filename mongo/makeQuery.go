package mongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"reflect"
	"time"
)

type MongoStruct struct {
	Field   string    `json:"field,omitempty"`
	Array   []string  `json:"array,omitempty"`
	Integer int       `json:"integer,omitempty"`
	Ptr     *string   `json:"ptr,omitempty"`
	DtRange DateRange `json:"dtRange,omitempty"`
}

type DateRange struct {
	From *time.Time `json:"$gte,omitempty"`
	To   *time.Time `json:"$lte,omitempty"`
}

func BuildBsonQuery(search map[string]interface{}) bson.D {
	var terms []bson.D
	query := bson.D{}
	for key, v := range search {
		if reflect.ValueOf(v).Kind() == reflect.Slice {
			in := bson.D{bson.E{Key: "$in", Value: v}}
			terms = append(terms, bson.D{bson.E{Key: key, Value: in}})
			continue
		}
		if v != nil {
			singleTerm := bson.D{bson.E{Key: key, Value: v}}
			terms = append(terms, singleTerm)
		}
	}
	query = append(query, bson.E{Key: "$and", Value: terms})
	return query
}
