package mongo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

// abstract for mongo client to use the functions
type MongoClient struct {
	*mongo.Client
}

func NewMongoClient(mongoConnection string) (*MongoClient, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoConnection))
	if err != nil {
		return nil, err
	}
	return &MongoClient{
		client,
	}, nil

}

func (m *MongoClient) PingServer() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err := m.Connect(ctx)

	return err
}

func (m *MongoClient) ConnectClient() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err := m.Connect(ctx)

	return err
}

func (m *MongoClient) InsertOne(db, collection string, options options.InsertOneOptions, insert interface{}) error {
	coll := m.Database(db).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := coll.InsertOne(ctx, insert, &options)
	//result is the struct with the inserted object id
	if err != nil {
		fmt.Printf("Fail to insert obj %s", result.InsertedID)
	}
	return err
}

func (m *MongoClient) InsertMany(db, collection string, options options.InsertManyOptions, insert []interface{}) error {
	coll := m.Database(db).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, err := coll.InsertMany(ctx, insert, &options)
	//result is the struct with the inserted object ids
	if err != nil {
		for id := range result.InsertedIDs {
			fmt.Printf("Fail to insert obj %s\n", id)
		}
	}
	return err
}

// FindMany uses an abstraction of the find function with a cursor
func (m *MongoClient) FindMany(database, collection string, selector bson.D, options options.FindOptions, output interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cur, err := coll.Find(ctx, selector, &options)
	if err == mongo.ErrNoDocuments {
		//handle the no record found
	} else if err != nil {
		return err
	}
	err = cur.All(ctx, output)
	if err != nil {
		//handle errors
	}
	return err
}

// FindMany uses an abstraction of the find function with a cursor
func (m *MongoClient) FindOne(database, collection string, selector bson.D, options options.FindOptions, output interface{}) error {
	coll := m.Database(database).Collection(collection)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err := coll.FindOne(ctx, selector).Decode(output)
	if err == mongo.ErrNoDocuments {
		//handle the no record found
	} else if err != nil {
		//handle errors
	}
	return err
}

func (m *MongoClient) UpdateOne(database, collection string, selector bson.D, options options.UpdateOptions, update interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.UpdateOne(ctx, selector, update, &options)
	//the update object contains useful information that you may want to use
	if err != nil {
		return err
	}
	return nil
}

func (m *MongoClient) UpdateMany(database, collection string, selector bson.D, options options.UpdateOptions, update interface{}) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.UpdateMany(ctx, selector, update, &options)
	//the update object contains useful information that you may want to use
	if err != nil {
		return err
	}
	return nil
}

func (m *MongoClient) DeleteOne(database, collection string, options options.DeleteOptions, selector bson.D) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.DeleteOne(ctx, selector, &options)
	if err != nil {
		//handle error
	}
	return err
}

func (m *MongoClient) DeleteMany(database, collection string, options options.DeleteOptions, selector bson.D) error {
	coll := m.Database(database).Collection(collection)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := coll.DeleteMany(ctx, selector, &options)
	//The return object from Delete contains the count of the elements deleted
	if err != nil {
		//handle error
	}
	return err
}
